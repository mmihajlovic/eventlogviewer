﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventLogViewer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(args));
        }

        /// <summary>
        /// From http://stackoverflow.com/questions/4322542/exception-handling-within-a-linq-expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        public static IEnumerable<T> SkipExceptions<T>(this IEnumerable<T> values)
        {
            using (var enumerator = values.GetEnumerator())
            {
                bool next = true;
                while (next)
                {
                    try
                    {
                        next = enumerator.MoveNext();
                    }
                    catch
                    {
                        continue;
                    }

                    if (next)
                    {
                        yield return enumerator.Current;
                    }
                }
            }
        }

        public static void HighlightText(this RichTextBox myRtb, string word, Color foreColor, Color backColor)
        {
            if (myRtb == null) return;
            if (string.IsNullOrEmpty(word)) return;

            int s_start = myRtb.SelectionStart, startIndex = 0, index;

            while ((index = myRtb.Text.IndexOf(word, startIndex, StringComparison.InvariantCultureIgnoreCase)) != -1)
            {
                myRtb.Select(index, word.Length);
                myRtb.SelectionColor = foreColor;
                myRtb.SelectionBackColor = backColor;
                myRtb.SelectionFont = new Font(myRtb.SelectionFont, FontStyle.Bold);

                startIndex = index + word.Length;
            }

            //myRtb.SelectionStart = s_start;
            myRtb.SelectionLength = 0;
            //myRtb.SelectionColor = Color.Black;
        }
    }
}
