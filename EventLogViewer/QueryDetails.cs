﻿using System;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;

namespace EventLogViewer
{
    public class QueryDetails
    {
        public long Count { get; private set; }
        public string Path { get; set; }
        public PathType PathType { get; private set; }
        public string Query { get; set; }

        private EventLogQuery eventLogQuery { get; set; }

        public EventLogQuery ToQuery()
        {
            //if (string.IsNullOrWhiteSpace(Query))
            eventLogQuery = new EventLogQuery(this.Path, this.PathType);
            //else
            //    eventLogQuery = new EventLogQuery(this.Path, this.PathType, this.Query);
            eventLogQuery.ReverseDirection = true;

            return eventLogQuery;
        }

        public void Clear()
        {
            if (PathType == PathType.LogName)
            {
                eventLogQuery.Session.ClearLog(Path);
            }
        }

        public QueryDetails(PathType pathType, EventLog log)
        {
            this.PathType = pathType;
            this.Path = log.Log;

            try
            {
                this.Count = log.Entries.Count;
            }
            catch
            {
                this.Count = 0;
            }
        }

        public QueryDetails(PathType pathType, string filename)
        {
            this.PathType = pathType;
            this.Path = filename;

            try
            {
                int count = 0;
                var r = new EventLogReader(filename, PathType.FilePath);
                while (r.ReadEvent(TimeSpan.FromSeconds(1)) != null)
                {
                    count++;
                }
                this.Count = count;
            }
            catch
            {
                this.Count = 0;
            }
        }
    }

}
