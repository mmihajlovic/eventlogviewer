﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventLogViewer
{
    public partial class Form1 : Form
    {
        private string title;
        private QueryDetails eventLogQuery;
        private System.Windows.Forms.Timer refreshTimer;

        const int padding = 5;

        public Form1()
        {
            InitializeComponent();

            typeof(DataGridView).InvokeMember(
                "DoubleBuffered",
                BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
                null,
                dataGridView1,
                new object[] { true });

            typeof(DataGridView).InvokeMember(
                "DoubleBuffered",
                BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.SetProperty,
                null,
                dataGridView2,
                new object[] { true });

            title = this.Text;

            dataGridView1.RowTemplate.Height = FontHeight * 3 + padding * 4;
        }

        public Form1(string[] args) : this()
        {
            if (args != null && args.Length > 0)
                OpenLogFile(args[0]);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            queryDetailsBindingSource.DataSource = EventLog.GetEventLogs()
                .Select(log => new QueryDetails(PathType.LogName, log))                
                .ToList();
        }

        private async void DataGridView2_SelectionChanged(object s, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            try
            {
                uiQueryText.Text = "";
                uiClearLog.Enabled = true;

                toolStrip1.Enabled = false;

                var grid = s as DataGridView;
                var item = queryDetailsBindingSource.Current as QueryDetails;
                eventLogQuery = item;
                var items = await ShowEvents(item, int.Parse(uiLimitRows.Text));

                eventLogRecordBindingSource.DataSource = items; //.OrderByDescending(r => r.RecordId).ToList();
                eventLogRecordBindingSource.MoveFirst();
                toolStripStatusLabel1.Text = items.Count.ToString() + " record(s)";

                this.Text = title + " " + item.Path;

                uiRefresh.Enabled = true;
            }
            finally
            {
                toolStrip1.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        private void uiOpenLogFile_Click(object sender, EventArgs e)
        {
            using (var d = new OpenFileDialog())
            {
                d.DefaultExt = "evtx";
                d.Filter = "EVTX Files|*.evtx|All Files|*.*";

                if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    OpenLogFile(d.FileName);
                }
            }
        }

        private async void OpenLogFile(string fileName)
        {
            this.Cursor = Cursors.WaitCursor;
            toolStrip1.Enabled = false;

            try
            {
                uiQueryText.Text = "";
                uiClearLog.Enabled = false;

                eventLogQuery = new QueryDetails(PathType.FilePath, fileName);
                queryDetailsBindingSource.Add(eventLogQuery);

                queryDetailsBindingSource.MoveLast();

                //var items = await ShowEvents(eventLogQuery, int.Parse(uiLimitRows.Text));

                //eventLogRecordBindingSource.DataSource = items; //.OrderByDescending(r => r.RecordId).ToList();
                //toolStripStatusLabel1.Text = items.Count.ToString() + " record(s)";

                //this.Text = title + " " + fileName;

                uiRefresh.Enabled = true;
            }
            finally
            {
                this.toolStrip1.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }

        private Task<List<EventRecord>> ShowEvents(QueryDetails query, int limitRows)
        {
            return Task.Run(() =>
            {
                // initial read
                var items = new List<EventRecord>();
                var beforeCulture = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                try
                {

                    using (var eventLogReader = new EventLogReader(query.ToQuery()))
                    {
                        var timer = Stopwatch.StartNew();
                        int count = 0;
                        while (eventLogReader.ReadEvent(TimeSpan.FromSeconds(1)) != null)
                            count++;
                        timer.Stop();

                        Debug.WriteLine("Took {0} for {1} events", +timer.Elapsed, count);

                        EventRecord ev;
                        int row = 0;
                        eventLogReader.Seek(SeekOrigin.Begin, 0);
                        while ((ev = eventLogReader.ReadEvent(TimeSpan.FromSeconds(1))) != null && row < limitRows)
                        {
                            row++;
                            items.Add(ev);
                        }
                    }
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = beforeCulture;
                }

                if (string.IsNullOrWhiteSpace(uiQueryText.Text) == false)
                {
                    var searchText = uiQueryText.Text;
                    //from http://stackoverflow.com/questions/7531557/why-does-eventrecord-formatdescription-return-null
                    //var beforeCulture = Thread.CurrentThread.CurrentCulture;
                    //Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                    //try
                    //{
                    items = (from i in items
                             let p = i.FormatDescription() //i.Properties.Select(p => p.Value.ToString()).Where(p => !string.IsNullOrEmpty(p)).Aggregate((p1, p2) => p1 + "\r\n" + p2)
                             where
                             (i.ProviderName.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0) ||
                             (p != null && p.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0) ||
                             //(i.LevelDisplayName != null && i.LevelDisplayName.ToString().IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0) ||
                             (i.LogName.IndexOf(searchText, StringComparison.InvariantCultureIgnoreCase) >= 0)
                             select i
                                 ).SkipExceptions().ToList();
                    //}
                    //finally
                    //{
                    //    Thread.CurrentThread.CurrentCulture = beforeCulture;
                    //}
                }

                return items;

                //dataGridView1.DataSource = items.OrderByDescending(r => r.RecordId).ToList();
                //toolStripStatusLabel1.Text = items.Count.ToString() + " record(s)";
            });
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            var grid = (DataGridView)sender;
            //var item = ((List<EventRecord>)grid.DataSource)[e.RowIndex];
            var item = grid.Rows[e.RowIndex].DataBoundItem as EventRecord;

            // combine all descriptions into one list separated by \r\n
            //textBox1.Text = item.Properties.Count > 0
            //    ? item.Properties.Select(p => p.Value.ToString()).Where(p => !string.IsNullOrEmpty(p)).Aggregate((p1, p2) => p1 + "\r\n" + p2)
            //    : "[NO DESCRIPTION]";

            textBox1.Clear();

            textBox1.Text = item.FormatDescription() ?? string.Join("\r\n", item.Properties.Select(p => p.Value.ToString()).Where(p => !string.IsNullOrEmpty(p)).ToArray());

            switch ((StandardEventLevel)item.Level)
            {
                case StandardEventLevel.Critical:
                case StandardEventLevel.Error:
                    textBox1.ForeColor = Color.Red;
                    textBox1.BackColor = Color.LavenderBlush;
                    break;

                case StandardEventLevel.Warning:
                    textBox1.ForeColor = Color.DarkOrange;
                    textBox1.BackColor = Color.LightGoldenrodYellow;
                    break;

                case StandardEventLevel.Informational:
                case StandardEventLevel.LogAlways:
                case StandardEventLevel.Verbose:
                default:
                    textBox1.ForeColor = SystemColors.WindowText;
                    textBox1.BackColor = SystemColors.Window;
                    break;
            }

            textBox1.HighlightText(uiQueryText.Text, Color.SlateGray, Color.Yellow);
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            //try
            //{
            var grid = (DataGridView)sender;
            //var ds = (List<EventRecord>)grid.DataSource;
            //var item = ds[e.RowIndex];
            if (e.RowIndex >= eventLogRecordBindingSource.Count) return; // prevent exception when changing to a list with less rows

            var item = grid.Rows[e.RowIndex].DataBoundItem as EventRecord;

            switch ((StandardEventLevel)item.Level)
            {
                case StandardEventLevel.Critical:
                case StandardEventLevel.Error:
                    e.CellStyle.ForeColor = Color.Red;
                    e.CellStyle.BackColor = Color.LavenderBlush;
                    break;

                case StandardEventLevel.Warning:
                    e.CellStyle.ForeColor = Color.DarkOrange;
                    e.CellStyle.BackColor = Color.LightGoldenrodYellow;
                    break;

                case StandardEventLevel.Informational:
                case StandardEventLevel.LogAlways:
                case StandardEventLevel.Verbose:
                default:
                    //e.CellStyle.fore
                    break;
            }

            //if (e.ColumnIndex == 0) // icon
            //{
            //    switch ((StandardEventLevel)item.Level)
            //    {
            //        case StandardEventLevel.Critical:
            //        case StandardEventLevel.Error:
            //            e.Value = Properties.Resources.error_16;
            //            break;

            //        case StandardEventLevel.Warning:
            //            e.Value = Properties.Resources.warning_16;
            //            break;

            //        case StandardEventLevel.Informational:
            //        case StandardEventLevel.LogAlways:
            //        case StandardEventLevel.Verbose:
            //        default:
            //            e.Value = Properties.Resources.information_16;
            //            break;
            //    }
            //}

            //highlight row
            //if (!string.IsNullOrEmpty(uiQueryText.Text) && (
            //        (item.FormatDescription() != null && item.FormatDescription().IndexOf(uiQueryText.Text, StringComparison.InvariantCultureIgnoreCase) != -1))
            //       )
            //{
            //    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);
            //}


            //highlight cell
            if (!string.IsNullOrEmpty(uiQueryText.Text))
            {
                var levelFound = item.LevelDisplayName != null && item.LevelDisplayName.IndexOf(uiQueryText.Text, StringComparison.InvariantCultureIgnoreCase) != -1;
                var providerFound = item.ProviderName != null && item.ProviderName.IndexOf(uiQueryText.Text, StringComparison.InvariantCultureIgnoreCase) != -1;

                var msgStr = item.FormatDescription() ?? string.Join("\r\n", item.Properties.Select(p => p.Value.ToString()).Where(p => !string.IsNullOrEmpty(p)).ToArray());
                var msgFound = msgStr != null && msgStr.IndexOf(uiQueryText.Text, StringComparison.InvariantCultureIgnoreCase) != -1;

                if (levelFound || providerFound || msgFound)
                {
                    e.CellStyle.Font = new Font(e.CellStyle.Font, FontStyle.Bold);

                    e.CellStyle.ForeColor = Color.SlateGray;
                    e.CellStyle.BackColor = Color.Yellow;
                }
            }            

            //if (e.ColumnIndex == 3)  //time
            //{
            //    e.Value = ((DateTime)e.Value).ToString("yyyy-MM-dd HH:mm:ss.fff");
            //}
            //}
            //catch
            //{
            //    if (e.ColumnIndex == 0)
            //        e.Value = null;
            //    // nothing to be done - ignore
            //}
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // simple numeric constraint
            switch (e.KeyChar)
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '\b': // backspace
                    e.Handled = false;
                    break;

                default:
                    e.Handled = true;
                    break;
            }
        }

        private void Form1_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (files.Length == 1 && Path.GetExtension(files[0]).ToLower() == ".evtx")
                    e.Effect = DragDropEffects.Copy;
            }
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            var files = (string[])e.Data.GetData(DataFormats.FileDrop);

            OpenLogFile(files[0]);
        }

        private void uiWordWrap_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.WordWrap = uiWordWrap.Checked;
        }

        private async void uiRefresh_Click(object sender, EventArgs e)
        {
            if (eventLogQuery != null)
            {
                this.Cursor = Cursors.WaitCursor;
                this.toolStrip1.Enabled = false;

                var prevSelected = dataGridView1.CurrentRow != null ? dataGridView1.CurrentRow.Index : -1;

                try
                {
                    var items = await ShowEvents(eventLogQuery, int.Parse(uiLimitRows.Text));

                    textBox1.Text = string.Empty;
                    eventLogRecordBindingSource.DataSource = items;
                    toolStripStatusLabel1.Text = items.Count.ToString() + " record(s)";

                    if (prevSelected > -1 && dataGridView1.Rows.Count > prevSelected)
                    {
                        dataGridView1.CurrentCell = dataGridView1.Rows[prevSelected].Cells[0];
                    }
                }
                finally
                {
                    this.toolStrip1.Enabled = true;
                    this.Cursor = Cursors.Default;
                }
            }
        }

        private void uiQueryHelp_Click(object sender, EventArgs e)
        {
            Process.Start(@"http://msdn.microsoft.com/en-us/library/bb399427(v=vs.90).aspx");
        }

        private void uiClearLog_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Are you sure you would like to clear the " + eventLogQuery.Path + " log?", "Clear log", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                eventLogQuery.Clear();
                uiRefresh.PerformButtonClick();
            }
        }

        private void uiQueryText_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (eventLogQuery == null)
                return;

            if (e.KeyChar == "\r"[0])
            {
                uiSearch.PerformClick();
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }

        private void uiRefresh5Seconds_Click(object sender, EventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Windows.Forms.Timer();
                refreshTimer.Tick += refreshTimer_Tick;
            }

            refreshTimer.Interval = 5000;
            refreshTimer.Start();

            foreach (ToolStripMenuItem button in uiRefresh.DropDownItems)
            {
                if (button == uiRefresh5Seconds)
                    button.Checked = true;
                else
                    button.Checked = false;
            }

            uiRefresh.Image = Properties.Resources.clock_history_frame;
            uiRefresh.ToolTipText = "Auto refresh every 5 seconds";
        }

        private void uiRefresh1Min_Click(object sender, EventArgs e)
        {
            if (refreshTimer == null)
            {
                refreshTimer = new System.Windows.Forms.Timer();
                refreshTimer.Tick += refreshTimer_Tick;
            }

            refreshTimer.Interval = 60000;
            refreshTimer.Start();

            foreach (ToolStripMenuItem button in uiRefresh.DropDownItems)
            {
                if (button == uiRefresh1Min)
                    button.Checked = true;
                else
                    button.Checked = false;
            }

            uiRefresh.Image = Properties.Resources.clock_history_frame;
            uiRefresh.ToolTipText = "Auto refresh every 1 minute";
        }

        private void uiManualRefresh_Click(object sender, EventArgs e)
        {
            if (refreshTimer != null)
            {
                refreshTimer.Stop();
                refreshTimer = null;
            }

            foreach (ToolStripMenuItem button in uiRefresh.DropDownItems)
            {
                if (button == uiManualRefresh)
                    button.Checked = true;
                else
                    button.Checked = false;
            }

            uiRefresh.Image = Properties.Resources.arrow_refresh;
            uiRefresh.ToolTipText = "Refresh";
        }

        void refreshTimer_Tick(object sender, EventArgs e)
        {
            uiRefresh.PerformButtonClick();
        }

        private void uiSearch_Click(object sender, EventArgs e)
        {
            if (eventLogQuery == null) return;

            eventLogQuery.Query = uiQueryText.Text;

            uiRefresh.PerformButtonClick();
            uiClearSearch.Enabled = true;
        }

        private void uiClearSearch_Click(object sender, EventArgs e)
        {
            uiSearch.Text = string.Empty;
            eventLogQuery.Query = "";

            uiRefresh.PerformButtonClick();
            uiClearSearch.Enabled = false;
        }

        private void uiVertSplit_CheckedChanged(object sender, EventArgs e)
        {
            splitContainer1.Orientation = uiVertSplit.Checked ? Orientation.Vertical : Orientation.Horizontal;
        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == colItems_Custom.Index)
                {
                    var grid = sender as DataGridView;
                    var log = grid.Rows[e.RowIndex].DataBoundItem as EventRecord;
                    var selected = grid.SelectedRows.Count > 0 && grid.SelectedRows[0].Index == e.RowIndex;
                    var foreColor = selected ? e.CellStyle.SelectionForeColor : e.CellStyle.ForeColor;
                    var grayColor = selected ? e.CellStyle.SelectionForeColor : SystemColors.ControlDark;
                    var brush = new SolidBrush(foreColor);

                    e.PaintBackground(e.CellBounds, selected);

                    var img = (StandardEventLevel)log.Level == StandardEventLevel.Critical ? Properties.Resources.error_16
                        : (StandardEventLevel)log.Level == StandardEventLevel.Error ? Properties.Resources.error_16
                        : (StandardEventLevel)log.Level == StandardEventLevel.Informational ? Properties.Resources.information_16
                        : (StandardEventLevel)log.Level == StandardEventLevel.LogAlways ? Properties.Resources.information_16
                        : (StandardEventLevel)log.Level == StandardEventLevel.Verbose ? Properties.Resources.information_16
                        : (StandardEventLevel)log.Level == StandardEventLevel.Warning ? Properties.Resources.warning_16
                        : null;

                    var line1Rect = new Rectangle(e.CellBounds.X + img.Width + padding * 2, e.CellBounds.Y + padding, e.CellBounds.Width - img.Width - padding * 3, FontHeight);
                    var line2Rect = new Rectangle(line1Rect.Location, line1Rect.Size); line2Rect.Offset(0, FontHeight + padding);
                    var line3Rect = new Rectangle(line2Rect.Location, line2Rect.Size); line3Rect.Offset(0, FontHeight + padding);

                    // IMAGE
                    e.Graphics.DrawImage(img, e.CellBounds.X + padding, e.CellBounds.Y + padding);

                    // DATE
                    var dateStr = log.TimeCreated.Value.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var dateSize = TextRenderer.MeasureText(dateStr, e.CellStyle.Font);
                    var dateRect = new Rectangle(line1Rect.Right - dateSize.Width, line1Rect.Y, dateSize.Width, line1Rect.Height);
                    if (dateRect.Width > line1Rect.Width) dateRect.Width = line1Rect.Width;
                    dateRect.X = Math.Max(dateRect.X, line1Rect.X);

                    TextRenderer.DrawText(e.Graphics, dateStr, e.CellStyle.Font, dateRect, foreColor, TextFormatFlags.EndEllipsis);

                    // lEVEL
                    var levelRect = new Rectangle(line1Rect.X, line1Rect.Y, line1Rect.Width - dateRect.Width - padding, line1Rect.Height);
                    //e.Graphics.DrawString(log.LevelDisplayName, new Font(this.Font, FontStyle.Bold), brush, e.CellBounds.X + img.Width + padding * 2, e.CellBounds.Y + padding);
                    TextRenderer.DrawText(e.Graphics, log.LevelDisplayName, new Font(e.CellStyle.Font, FontStyle.Bold), levelRect, foreColor, TextFormatFlags.EndEllipsis);

                    // PROVIDER
                    var providerSize = TextRenderer.MeasureText(log.ProviderName, e.CellStyle.Font);
                    var providerRect = new Rectangle(line2Rect.X, line2Rect.Y, Math.Min(line2Rect.Width, providerSize.Width), line2Rect.Height);
                    TextRenderer.DrawText(e.Graphics, log.ProviderName, e.CellStyle.Font, providerRect, foreColor, TextFormatFlags.EndEllipsis);

                    // ID
                    var idStr = log.Id.ToString(@"\#0");
                    var idSize = TextRenderer.MeasureText(idStr, e.CellStyle.Font);
                    var idRect = new Rectangle(line2Rect.Right - idSize.Width, line2Rect.Y, idSize.Width, line2Rect.Height);
                    idRect.X = Math.Max(idRect.X, line2Rect.X);

                    if (idRect.IntersectsWith(providerRect) == false)
                    {
                        TextRenderer.DrawText(e.Graphics, idStr, e.CellStyle.Font, idRect, grayColor, TextFormatFlags.Default);
                    }

                    // SUMMARY
                    var msgStr = log.FormatDescription() ?? string.Join("\r\n", log.Properties.Select(p => p.Value.ToString()).Where(p => !string.IsNullOrEmpty(p)).ToArray());
                    msgStr = msgStr.Split(new[] { '\r', '\n' }).FirstOrDefault() ?? msgStr; //return first line

                    var msgSize = TextRenderer.MeasureText(msgStr, e.CellStyle.Font, new Size(e.CellBounds.Width - img.Width - padding * 3, FontHeight), TextFormatFlags.HorizontalCenter |
                      TextFormatFlags.EndEllipsis | TextFormatFlags.SingleLine | TextFormatFlags.EndEllipsis | TextFormatFlags.ModifyString);
                    var msgRect = line3Rect;

                    //e.Graphics.DrawString(msgStr, this.Font, SystemBrushes.ControlDark, msgRect);
                    TextRenderer.DrawText(e.Graphics, msgStr, e.CellStyle.Font, msgRect, grayColor, TextFormatFlags.EndEllipsis);

                    e.Handled = true;
                }
            }
            catch
            {
                e.Handled = false;
            }
        }

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var grid = (DataGridView)sender;
            //var ds = (List<EventRecord>)grid.DataSource;
            //var item = ds[e.RowIndex];
            if (e.RowIndex >= queryDetailsBindingSource.Count) return; // prevent exception when changing to a list with less rows

            var item = grid.Rows[e.RowIndex].DataBoundItem as QueryDetails;

            if (e.ColumnIndex == 0) // icon
            {
                switch (item.PathType)
                {
                    case PathType.FilePath:
                        e.Value = Properties.Resources.file_extension_log;
                        break;
                    default:
                        e.Value = Properties.Resources.book;
                        break;
                }

                return;
            }

            if (e.ColumnIndex == 1) // name
            {
                return;
            }

            if (e.ColumnIndex == 2) // count
            {
                e.CellStyle.ForeColor = SystemColors.ControlDark;
                return;
            }

        }

        private void dataGridView2_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            var grid = (DataGridView)sender;
            //var ds = (List<EventRecord>)grid.DataSource;
            //var item = ds[e.RowIndex];
            if (e.RowIndex >= queryDetailsBindingSource.Count) return; // prevent exception when changing to a list with less rows

            var item = grid.Rows[e.RowIndex].DataBoundItem as QueryDetails;

            e.ToolTipText = item.Path;
        }

        private void dataGridView2_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == 1) // name
            {
                var grid = sender as DataGridView;
                var query = grid.Rows[e.RowIndex].DataBoundItem as QueryDetails;
                var selected = grid.SelectedRows.Count > 0 && grid.SelectedRows[0].Index == e.RowIndex;
                var foreColor = selected ? e.CellStyle.SelectionForeColor : e.CellStyle.ForeColor;

                e.PaintBackground(e.ClipBounds, selected);

                var flags = TextFormatFlags.VerticalCenter | TextFormatFlags.PathEllipsis | TextFormatFlags.EndEllipsis;

                TextRenderer.DrawText(e.Graphics, (string)e.Value, e.CellStyle.Font, e.CellBounds, foreColor, flags);
                e.Handled = true;
            }
        }
    }
}
