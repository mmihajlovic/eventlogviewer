﻿namespace EventLogViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colItems_Custom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventLogRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.RichTextBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.uiWordWrap = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.uiOpenLogFile = new System.Windows.Forms.ToolStripButton();
            this.uiOpenSystemLog = new System.Windows.Forms.ToolStripDropDownButton();
            this.uiClearLog = new System.Windows.Forms.ToolStripButton();
            this.uiRefresh = new System.Windows.Forms.ToolStripSplitButton();
            this.uiManualRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.uiRefresh5Seconds = new System.Windows.Forms.ToolStripMenuItem();
            this.uiRefresh1Min = new System.Windows.Forms.ToolStripMenuItem();
            this.uiVertSplit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.uiLimitRows = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.uiQueryText = new System.Windows.Forms.ToolStripTextBox();
            this.uiSearch = new System.Windows.Forms.ToolStripButton();
            this.uiClearSearch = new System.Windows.Forms.ToolStripButton();
            this.uiQueryHelp = new System.Windows.Forms.ToolStripButton();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.colLogList_Image = new System.Windows.Forms.DataGridViewImageColumn();
            this.colLogList_Log = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLogList_Count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queryDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.queryDetailsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colItems_Custom});
            this.dataGridView1.DataSource = this.eventLogRecordBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ControlLight;
            this.dataGridView1.Location = new System.Drawing.Point(5, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 48;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(627, 301);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView1_CellPainting);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            // 
            // colItems_Custom
            // 
            this.colItems_Custom.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colItems_Custom.HeaderText = "custom";
            this.colItems_Custom.Name = "colItems_Custom";
            this.colItems_Custom.ReadOnly = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.toolStrip2);
            this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.splitContainer1.Size = new System.Drawing.Size(637, 482);
            this.splitContainer1.SplitterDistance = 309;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(5, 29);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(1);
            this.panel1.Size = new System.Drawing.Size(627, 137);
            this.panel1.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(1, 1);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(623, 133);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "";
            // 
            // toolStrip2
            // 
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uiWordWrap});
            this.toolStrip2.Location = new System.Drawing.Point(5, 4);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(627, 25);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // uiWordWrap
            // 
            this.uiWordWrap.Checked = true;
            this.uiWordWrap.CheckOnClick = true;
            this.uiWordWrap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uiWordWrap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uiWordWrap.Image = global::EventLogViewer.Properties.Resources.word_wrap;
            this.uiWordWrap.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiWordWrap.Name = "uiWordWrap";
            this.uiWordWrap.Size = new System.Drawing.Size(23, 22);
            this.uiWordWrap.Text = "toolStripButton1";
            this.uiWordWrap.CheckedChanged += new System.EventHandler(this.uiWordWrap_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 507);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(829, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(63, 17);
            this.toolStripStatusLabel1.Text = "0 record(s)";
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uiOpenLogFile,
            this.uiOpenSystemLog,
            this.uiClearLog,
            this.uiRefresh,
            this.uiVertSplit,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.uiLimitRows,
            this.toolStripSeparator2,
            this.toolStripLabel2,
            this.uiQueryText,
            this.uiSearch,
            this.uiClearSearch,
            this.uiQueryHelp});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(829, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // uiOpenLogFile
            // 
            this.uiOpenLogFile.Image = global::EventLogViewer.Properties.Resources.book_open;
            this.uiOpenLogFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiOpenLogFile.Name = "uiOpenLogFile";
            this.uiOpenLogFile.Size = new System.Drawing.Size(100, 22);
            this.uiOpenLogFile.Text = "Open Log File";
            this.uiOpenLogFile.Click += new System.EventHandler(this.uiOpenLogFile_Click);
            // 
            // uiOpenSystemLog
            // 
            this.uiOpenSystemLog.Image = global::EventLogViewer.Properties.Resources.books_stack;
            this.uiOpenSystemLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiOpenSystemLog.Name = "uiOpenSystemLog";
            this.uiOpenSystemLog.Size = new System.Drawing.Size(137, 22);
            this.uiOpenSystemLog.Text = "Open Machine Log";
            this.uiOpenSystemLog.Visible = false;
            // 
            // uiClearLog
            // 
            this.uiClearLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uiClearLog.Enabled = false;
            this.uiClearLog.Image = global::EventLogViewer.Properties.Resources.draw_eraser;
            this.uiClearLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiClearLog.Name = "uiClearLog";
            this.uiClearLog.Size = new System.Drawing.Size(23, 22);
            this.uiClearLog.Text = "Clear Log";
            this.uiClearLog.Click += new System.EventHandler(this.uiClearLog_Click);
            // 
            // uiRefresh
            // 
            this.uiRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uiRefresh.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uiManualRefresh,
            this.uiRefresh5Seconds,
            this.uiRefresh1Min});
            this.uiRefresh.Enabled = false;
            this.uiRefresh.Image = global::EventLogViewer.Properties.Resources.arrow_refresh;
            this.uiRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiRefresh.Name = "uiRefresh";
            this.uiRefresh.Size = new System.Drawing.Size(32, 22);
            this.uiRefresh.Text = "Refresh";
            this.uiRefresh.ButtonClick += new System.EventHandler(this.uiRefresh_Click);
            // 
            // uiManualRefresh
            // 
            this.uiManualRefresh.Checked = true;
            this.uiManualRefresh.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uiManualRefresh.Image = global::EventLogViewer.Properties.Resources.arrow_refresh;
            this.uiManualRefresh.Name = "uiManualRefresh";
            this.uiManualRefresh.Size = new System.Drawing.Size(176, 22);
            this.uiManualRefresh.Text = "Manual Refresh";
            this.uiManualRefresh.Click += new System.EventHandler(this.uiManualRefresh_Click);
            // 
            // uiRefresh5Seconds
            // 
            this.uiRefresh5Seconds.Image = global::EventLogViewer.Properties.Resources.clock_history_frame;
            this.uiRefresh5Seconds.Name = "uiRefresh5Seconds";
            this.uiRefresh5Seconds.Size = new System.Drawing.Size(176, 22);
            this.uiRefresh5Seconds.Text = "Auto Refresh 5 secs";
            this.uiRefresh5Seconds.Click += new System.EventHandler(this.uiRefresh5Seconds_Click);
            // 
            // uiRefresh1Min
            // 
            this.uiRefresh1Min.Image = global::EventLogViewer.Properties.Resources.clock_history_frame;
            this.uiRefresh1Min.Name = "uiRefresh1Min";
            this.uiRefresh1Min.Size = new System.Drawing.Size(176, 22);
            this.uiRefresh1Min.Text = "Auto Refresh 1 min";
            this.uiRefresh1Min.Click += new System.EventHandler(this.uiRefresh1Min_Click);
            // 
            // uiVertSplit
            // 
            this.uiVertSplit.CheckOnClick = true;
            this.uiVertSplit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uiVertSplit.Image = global::EventLogViewer.Properties.Resources.split_panel;
            this.uiVertSplit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiVertSplit.Name = "uiVertSplit";
            this.uiVertSplit.Size = new System.Drawing.Size(23, 22);
            this.uiVertSplit.Text = "toolStripButton1";
            this.uiVertSplit.CheckedChanged += new System.EventHandler(this.uiVertSplit_CheckedChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(65, 22);
            this.toolStripLabel1.Text = "Limit Rows";
            // 
            // uiLimitRows
            // 
            this.uiLimitRows.Name = "uiLimitRows";
            this.uiLimitRows.Size = new System.Drawing.Size(116, 25);
            this.uiLimitRows.Text = "100000";
            this.uiLimitRows.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(42, 22);
            this.toolStripLabel2.Text = "Search";
            // 
            // uiQueryText
            // 
            this.uiQueryText.Name = "uiQueryText";
            this.uiQueryText.Size = new System.Drawing.Size(116, 25);
            this.uiQueryText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uiQueryText_KeyPress);
            // 
            // uiSearch
            // 
            this.uiSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uiSearch.Image = global::EventLogViewer.Properties.Resources.filter;
            this.uiSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiSearch.Name = "uiSearch";
            this.uiSearch.Size = new System.Drawing.Size(23, 22);
            this.uiSearch.Text = "Search";
            this.uiSearch.Click += new System.EventHandler(this.uiSearch_Click);
            // 
            // uiClearSearch
            // 
            this.uiClearSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uiClearSearch.Enabled = false;
            this.uiClearSearch.Image = global::EventLogViewer.Properties.Resources.filter_clear;
            this.uiClearSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiClearSearch.Name = "uiClearSearch";
            this.uiClearSearch.Size = new System.Drawing.Size(23, 22);
            this.uiClearSearch.Text = "Clear Search";
            this.uiClearSearch.Click += new System.EventHandler(this.uiClearSearch_Click);
            // 
            // uiQueryHelp
            // 
            this.uiQueryHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.uiQueryHelp.Image = global::EventLogViewer.Properties.Resources.emotion_question;
            this.uiQueryHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.uiQueryHelp.Name = "uiQueryHelp";
            this.uiQueryHelp.Size = new System.Drawing.Size(23, 22);
            this.uiQueryHelp.Text = "Query Help";
            this.uiQueryHelp.Visible = false;
            this.uiQueryHelp.Click += new System.EventHandler(this.uiQueryHelp_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 25);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView2);
            this.splitContainer2.Panel1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainer2.Size = new System.Drawing.Size(829, 482);
            this.splitContainer2.SplitterDistance = 189;
            this.splitContainer2.SplitterWidth = 3;
            this.splitContainer2.TabIndex = 3;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeColumns = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.ColumnHeadersVisible = false;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colLogList_Image,
            this.colLogList_Log,
            this.colLogList_Count});
            this.dataGridView2.DataSource = this.queryDetailsBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(5, 4);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.RowTemplate.Height = 30;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(179, 474);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView2_CellFormatting);
            this.dataGridView2.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView2_CellPainting);
            this.dataGridView2.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dataGridView2_CellToolTipTextNeeded);
            this.dataGridView2.SelectionChanged += new System.EventHandler(this.DataGridView2_SelectionChanged);
            // 
            // colLogList_Image
            // 
            this.colLogList_Image.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.colLogList_Image.HeaderText = "";
            this.colLogList_Image.Image = global::EventLogViewer.Properties.Resources.book;
            this.colLogList_Image.Name = "colLogList_Image";
            this.colLogList_Image.ReadOnly = true;
            this.colLogList_Image.Width = 5;
            // 
            // colLogList_Log
            // 
            this.colLogList_Log.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colLogList_Log.DataPropertyName = "Path";
            this.colLogList_Log.HeaderText = "Name";
            this.colLogList_Log.Name = "colLogList_Log";
            this.colLogList_Log.ReadOnly = true;
            // 
            // colLogList_Count
            // 
            this.colLogList_Count.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.colLogList_Count.DataPropertyName = "Count";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0;; ";
            dataGridViewCellStyle2.NullValue = null;
            this.colLogList_Count.DefaultCellStyle = dataGridViewCellStyle2;
            this.colLogList_Count.HeaderText = "Count";
            this.colLogList_Count.Name = "colLogList_Count";
            this.colLogList_Count.ReadOnly = true;
            this.colLogList_Count.Width = 5;
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 529);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Event Log Viewer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.Form1_DragOver);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLogRecordBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.queryDetailsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource eventLogRecordBindingSource;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton uiOpenLogFile;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripDropDownButton uiOpenSystemLog;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox uiLimitRows;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton uiWordWrap;
        private System.Windows.Forms.ToolStripSplitButton uiRefresh;
        private System.Windows.Forms.ToolStripMenuItem uiRefresh5Seconds;
        private System.Windows.Forms.ToolStripMenuItem uiRefresh1Min;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripTextBox uiQueryText;
        private System.Windows.Forms.ToolStripButton uiQueryHelp;
        private System.Windows.Forms.ToolStripButton uiClearLog;
        private System.Windows.Forms.RichTextBox textBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem uiManualRefresh;
        private System.Windows.Forms.ToolStripButton uiSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton uiClearSearch;
        private System.Windows.Forms.ToolStripButton uiVertSplit;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.BindingSource queryDetailsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItems_Custom;
        private System.Windows.Forms.DataGridViewImageColumn colLogList_Image;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLogList_Log;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLogList_Count;
    }
}

