# Event Log Viewer v2.0
[![Build status](https://ci.appveyor.com/api/projects/status/ig4wqruigsmilqte)](https://ci.appveyor.com/project/mmihajlovic/eventlogviewer) [![Project Stats](https://www.openhub.net/p/eventlogviewer/widgets/project_thin_badge.gif)](https://www.openhub.net/p/eventlogviewer)

Viewer for windows event logs and evtx files. Colours info, errors and warnings and had filtering and search functionality.

![v2.0 Screenshot](https://bitbucket.org/repo/bRzq6A/images/3479950202-screenshot.png)

Download from the [Downloads tab](https://bitbucket.org/mmihajlovic/eventlogviewer/downloads)